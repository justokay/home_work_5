package misyac.yuri.home_work_5;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import misyac.yuri.home_work_5.adapter.MyAdapter;

public class MainActivity extends AppCompatActivity {

    private MyAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(String.valueOf(0));

        initRecycleView();
    }

    private void initRecycleView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false);
        assert recyclerView != null;
        recyclerView.setLayoutManager(layoutManager);
        List<String> list = getAdapterList();
        int space = getResources().getDimensionPixelOffset(R.dimen.recycle_space);
        recyclerView.addItemDecoration(new SpacesItemDecoration(space));
        mAdapter = new MyAdapter(this, list);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setSelectListener(new MyAdapter.OnItemSelectListener() {
            @Override
            public void onSelect(int count) {
                getSupportActionBar().setTitle(String.valueOf(count));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                mAdapter.addNewItem();
                return true;
            case R.id.action_delete:
                mAdapter.deleteSelected();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private List<String> getAdapterList() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add("Row " + (i + 1));
        }
        return list;
    }


    private class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int mSpace;

        SpacesItemDecoration(int space) {
            this.mSpace = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.right = mSpace;
            outRect.left = mSpace;
            outRect.top = mSpace;
            outRect.bottom = mSpace;
        }
    }
}
