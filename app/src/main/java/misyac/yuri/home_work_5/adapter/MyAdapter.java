package misyac.yuri.home_work_5.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import misyac.yuri.home_work_5.R;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{

    private static final int ODD = 0;
    private static final int EVEN = 1;
    private final Context mContext;

    private final List<String> mList;
    private SparseBooleanArray mSelected = new SparseBooleanArray();
    private OnItemSelectListener mSelectListener;

    public MyAdapter(Context context, List<String> list) {
        mContext = context;
        mList = list;
        initSparse();
    }

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ODD) {
            return new OddViewHolder(
                    LayoutInflater.from(mContext).inflate(R.layout.list_item_odd, parent, false)
            );
        } else {
            return new EvenViewHolder(
                    LayoutInflater.from(mContext).inflate(R.layout.list_item_even, parent, false)
            );
        }
    }

    @Override
    public void onBindViewHolder(MyAdapter.ViewHolder holder, int position) {
        if (holder.getItemViewType() == ODD) {
            OddViewHolder oddViewHolder = (OddViewHolder) holder;
            oddViewHolder.getFirst().setText(mList.get(position));
        } else {
            EvenViewHolder evenViewHolder = (EvenViewHolder) holder;
            evenViewHolder.getFirst().setText(mList.get(position));
            evenViewHolder.getSecond().setText(mList.get(position));
        }

        holder.deselect();
        holder.setPosition(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if ((position + 1)% 2 == 0) {
            return EVEN;
        } else {
            return ODD;
        }
    }

    public void setSelectListener(OnItemSelectListener selectListener) {
        mSelectListener = selectListener;
    }

    private void initSparse() {
        for (int i = 0; i < mList.size(); i++) {
            mSelected.clear();
            mSelected.put(i, false);
        }
    }

    private View.OnClickListener mListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Integer position = ((Integer) view.getTag());
            boolean state = !mSelected.get(position);
            mSelected.put(position, state);
            view.setSelected(state);

            if (mSelectListener != null) mSelectListener.onSelect(getSelectedCount());
        }
    };

    private int getSelectedCount() {
        int count = 0;
        for (int i = 0; i < mList.size(); i++) {
            if (mSelected.get(i)) count++;
        }
        return count;
    }

    private List<String> getSelectedList() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < mList.size(); i++) {
            if (mSelected.get(i)) list.add(mList.get(i));
        }
        return list;
    }

    public void deleteSelected() {
        mList.removeAll(getSelectedList());

        initSparse();
        notifyDataSetChanged();
        mSelectListener.onSelect(0);
    }

    public void addNewItem() {
        int max = mList.size();
        mList.add("Row " + (max + 1));
        mSelected.put(mSelected.size(), false);
        notifyDataSetChanged();
    }

    private class OddViewHolder extends MyAdapter.ViewHolder {

        private TextView mFirst;

        OddViewHolder(View view) {
            super(view);
            mFirst = (TextView) view.findViewById(R.id.first);
        }

        TextView getFirst() {
            return mFirst;
        }
    }

    private class EvenViewHolder extends MyAdapter.ViewHolder {

        private TextView mFirst;
        private TextView mSecond;

        EvenViewHolder(View view) {
            super(view);
            mFirst = (TextView) view.findViewById(R.id.first);
            mSecond = (TextView) view.findViewById(R.id.second);
        }

        TextView getFirst() {
            return mFirst;
        }

        TextView getSecond() {
            return mSecond;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        View mView;

        ViewHolder(View view) {
            super(view);
            view.setOnClickListener(mListener);
            mView = view;
        }

        public void setPosition(int position) {
            mView.setTag(position);
        }

        public void deselect() {
            mView.setSelected(false);
        }
    }

    public interface OnItemSelectListener {
        void onSelect(int count);
    }
}
